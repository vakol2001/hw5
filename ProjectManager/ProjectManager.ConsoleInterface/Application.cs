﻿using ProjectManager.BL;
using ProjectManager.BL.Services;
using System;
using System.Threading.Tasks;

namespace ProjectManager.ConsoleInterface
{
    sealed class Application
    {
        readonly Manager _manager = new();

        public Task SetUpAsync() => _manager.DonwoadDataAsync(new ConnectService("https://localhost:44356/api/"));
        public void Start()
        {
            bool exit = false;

            do
            {
                try
                {
                    exit = InternalProcess();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong");
                    Console.WriteLine(e.Message);
                }


            } while (!exit);


        }

        private bool InternalProcess()
        {
            Console.WriteLine("Choose a menu item.");
            PrintMenu();
            int coise = int.Parse(Console.ReadLine());
            Execute(coise);
            return coise == 0;
        }

        private void PrintMenu()
        {
            Console.WriteLine("1 -\tGetProjectInfoByUserId\t\t\t\ttask1");
            Console.WriteLine("2 -\tGetTasksWithShortNameByUserId\t\t\ttask2");
            Console.WriteLine("3 -\tGetTasksWichFinishedInCurrentYearByUserId\ttask3");
            Console.WriteLine("4 -\tGetTeamsWithParticipantsOlderThan10\t\ttask4");
            Console.WriteLine("5 -\tGetSortedUsersWithSortedTasks\t\t\ttask5");
            Console.WriteLine("6 -\tGetUserInfo\t\t\t\t\ttask6");
            Console.WriteLine("7 -\tGetProjectsInfo\t\t\t\t\ttask7");
            Console.WriteLine("0 -\tExit");
        }

        private void Execute(int coise)
        {
            switch (coise)
            {
                case 0:
                    break;
                case 1:
                    JsonPrinter.PrintObject(_manager.GetProjectInfoByUserId(GetId()));
                    break;
                case 2:
                    JsonPrinter.PrintObject(_manager.GetTasksWithShortNameByUserId(GetId()));
                    break;
                case 3:
                    JsonPrinter.PrintObject(_manager.GetTasksWichFinishedInCurrentYearByUserId(GetId()));
                    break;
                case 4:
                    JsonPrinter.PrintObject(_manager.GetTeamsWithParticipantsOlderThan10());
                    break;
                case 5:
                    JsonPrinter.PrintObject(_manager.GetSortedUsersWithSortedTasks());
                    break;
                case 6:
                    JsonPrinter.PrintObject(_manager.GetUserInfo(GetId()));
                    break;
                case 7:
                    JsonPrinter.PrintObject(_manager.GetProjectsInfo());
                    break;
                default:
                    Console.WriteLine("Uncorrect number");
                    break;
            }
        }

        private static int GetId()
        {
            Console.WriteLine("Enter id");
            int id = int.Parse(Console.ReadLine());
            return id;
        }
    }
}
