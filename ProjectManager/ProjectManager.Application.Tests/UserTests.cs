﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Infrastructure.Mapper.MappingProfiles;
using ProjectManager.Application.Users.Commands;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Linq;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace ProjectManager.Application.Tests
{
    public class UserTests : IDisposable
    {
        readonly ProjectManagerContext _context;
        readonly IMapper _mapper;
        public UserTests()
        {
            _mapper = new MapperConfiguration(conf =>
            {
                conf.AddProfile<ProjectMappingProfile>();
                conf.AddProfile<TaskMappingProfile>();
                conf.AddProfile<TeamMappingProfile>();
                conf.AddProfile<UserMappingProfile>();
            }).CreateMapper();

            var options = new DbContextOptionsBuilder<ProjectManagerContext>().UseInMemoryDatabase("TestUserDatabase").Options;
            _context = new ProjectManagerContext(options);
            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task CreateUserCommand_WhenAddUser_ThenUserInDb()
        {

            var handler = new CreateUserCommand.Handler(_mapper, _context);

            var request = new CreateUserCommand
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                BirthDay = new DateTime(2001, 02, 19),
                Email = "test@gmail.com",
                TeamId = 1,
            };

            var result = await handler.Handle(request, default);

            var user = _context.Users.Find(result.Id);

            Assert.Equal(request.FirstName, user.FirstName);
            Assert.Equal(request.LastName, user.LastName);
            Assert.Equal(request.TeamId, user.TeamId);
            Assert.Equal(request.Email, user.Email);
            Assert.Equal(request.BirthDay, user.BirthDay);
        }

        [Fact]
        public async Task CreateUserCommand_WhenAddUser_ThenReturnsUser()
        {

            var handler = new CreateUserCommand.Handler(_mapper, _context);

            var request = new CreateUserCommand
            {
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                BirthDay = new DateTime(2001, 02, 19),
                Email = "test@gmail.com",
                TeamId = 1,
            };

            var result = await handler.Handle(request, default);

            Assert.Equal(request.FirstName, result.FirstName);
            Assert.Equal(request.LastName, result.LastName);
            Assert.Equal(request.TeamId, result.TeamId);
            Assert.Equal(request.Email, result.Email);
            Assert.Equal(request.BirthDay, result.BirthDay);
        }

        [Fact]
        public async Task UpdateUserCommand_WhenAddUserToTeam_ThenTeamMembersPlusOne()
        {

            _context.Teams.Add(new Team
            {
                Id = 1,
                Name = "TestTeam",
                Participants = Enumerable.Range(1, 5).Select(x => new User { Id = x, FirstName = $"User{x}" }).ToList()
            });

            _context.Users.Add(new User { Id = 6, FirstName = "NewUser" });

            _context.SaveChanges();

            var handler = new UpdateUserCommand.Handler(_mapper, _context);

            var request = new UpdateUserCommand
            {
                Id = 6,
                TeamId = 1,
                FirstName = "NewUser"
            };

            var result = await handler.Handle(request, default);

            Assert.Equal(6, _context.Teams.Find(1).Participants.Count);

        }
    }
}
