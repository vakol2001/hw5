﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Queries
{
    public class GetProjectByIdQuery : IRequest<ProjectModel>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetProjectByIdQuery, ProjectModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<ProjectModel> Handle(GetProjectByIdQuery request, CancellationToken cancellationToken)
            {
                var project = await _context.Projects.FindAsync(new object[] { request.Id }, cancellationToken) 
                    ?? throw new KeyNotFoundException();

                var mappedProject = _mapper.Map<ProjectModel>(project);
                return mappedProject;
            }
        }
    }
}
