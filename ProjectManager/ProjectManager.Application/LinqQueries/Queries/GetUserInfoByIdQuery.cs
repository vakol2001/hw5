﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.LinqQueries.Models;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.LinqQueries.Queries
{
    public class GetUserInfoByIdQuery : IRequest<UserInfoModel>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetUserInfoByIdQuery, UserInfoModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UserInfoModel> Handle(GetUserInfoByIdQuery request, CancellationToken cancellationToken)
            {
                var userInfo = await _context.Projects.Where(p => p.AuthorId == request.Id)
                        .OrderBy(p => p.CreatedAt)
                        .Select(p => new
                        {
                            User = p.Author,
                            LastProject = p,
                            LastProjectTasks = p.Tasks,
                            NotFinishedTasks = _context.Tasks.Where(t => t.PerformerId == p.AuthorId && !t.FinishedAt.HasValue).ToList(),
                            LongestTask = _context.Tasks
                                                .Where(t => t.Id == request.Id)
                                                .OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt)
                                                .FirstOrDefault()
                        })
                        .LastOrDefaultAsync(cancellationToken);
                return new UserInfoModel
                {
                    User = _mapper.Map<UserModel>(userInfo.User),
                    LastProject = _mapper.Map<ProjectModel>(userInfo.LastProject),
                    LastProjectAmountTasks = userInfo.LastProjectTasks.Count,
                    AmountNotFinishedTasks = userInfo.NotFinishedTasks.Count,
                    LongestTask = _mapper.Map<TaskModel>(userInfo.LongestTask)
                };
            }
        }
    }
}
