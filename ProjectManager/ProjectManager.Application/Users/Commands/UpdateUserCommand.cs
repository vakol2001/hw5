﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Commands
{
    public class UpdateUserCommand : IRequest<UserModel>
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }


        public class Handler : IRequestHandler<UpdateUserCommand, UserModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<UserModel> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                _context.Entry(user).CurrentValues.SetValues(request);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<UserModel>(user);
            }
        }
    }
}
