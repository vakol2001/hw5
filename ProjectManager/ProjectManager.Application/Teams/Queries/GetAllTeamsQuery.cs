﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Queries
{
    public class GetAllTeamsQuery : IRequest<IEnumerable<TeamModel>>
    {
        public class Handler : IRequestHandler<GetAllTeamsQuery, IEnumerable<TeamModel>>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<IEnumerable<TeamModel>> Handle(GetAllTeamsQuery request, CancellationToken cancellationToken)
            {
                var teams = await _context.Teams.ToListAsync(cancellationToken);
                var mappedTeams = _mapper.Map<IEnumerable<TeamModel>>(teams);
                return mappedTeams;
            }
        }
    }
}
