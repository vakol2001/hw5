﻿using System.Collections.Generic;

namespace ProjectManager.Data.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<User> Participants { get; set; } = new HashSet<User>();
        public virtual ICollection<Project> Projects { get; set; } = new HashSet<Project>();
    }
}
