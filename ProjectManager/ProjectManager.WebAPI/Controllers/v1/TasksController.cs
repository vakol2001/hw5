﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProjectManager.Application.Tasks.Commands;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Application.Tasks.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManager.WebAPI.Controllers.v1
{
    public class TasksController : ApiController
    {
        public TasksController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllTasksQuery()));
        }

        [HttpGet("{Id}")]
        public async Task<ActionResult<TaskModel>> GetById([FromRoute] GetTaskByIdQuery query)
        {
            try
            {
                return Ok(await Mediator.Send(query));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }

        [HttpGet("Unfinished/{UserId}")]
        public async Task<ActionResult<TaskModel>> GetUnfinishedTasksByUserId([FromRoute] GetUnfinishedTasksByUserIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost]
        public async Task<ActionResult<TaskModel>> Update([FromBody] UpdateTaskCommand command)
        {
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
        [HttpPut]
        public async Task<ActionResult<TaskModel>> Put([FromBody] CreateTaskCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpDelete("{Id}")]
        public async Task<ActionResult> Delete([FromRoute] DeleteTaskCommand command)
        {
            try
            {
                await Mediator.Send(command);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

        }

    }
}
